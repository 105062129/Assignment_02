// Initialize Firebase
var config = {
    apiKey: "AIzaSyDHfujjrGt9XNzZNcpkAzMQX6YWqYxUCr8",
    authDomain: "as02-288c0.firebaseapp.com",
    databaseURL: "https://as02-288c0.firebaseio.com",
    projectId: "as02-288c0",
    storageBucket: "as02-288c0.appspot.com",
    messagingSenderId: "207247349991"
};
firebase.initializeApp(config);




var kScreenSize = [600, 600];
var kPlayerScale = [1.5, 1.5];
var kPlayerSpeed = 220;

var gStep = 2;
var gScore;
var gName = prompt("Type your name", "Foo") || "Foo";
var gAOver;
var gHardMode = false;
var gLaugh = ["Surprise", "UCCU", "wwwwwwww", "\\(^q^)/", "m9(OWO)", "No do no die", "Too young too simple", "Song La"];




function playAudio(obj, name) {
    //obj[name] = obj[name] || game.add.audio(name);
    //obj[name].play();

    let a = game.add.audio(name);
    a.play();
    setTimeout(() => { a.destroy(); }, 3000);
}




var bootState = {
    preload: function () {
        game.load.image("loadbar", "assets/loadbar.png", 600, 600);
    },

    create: function () {
        game.state.start("load");
    }
};

var loadState = {
    preload: function () {
        var bar = game.add.sprite(0, 0, "loadbar");
        game.load.setPreloadSprite(bar);

        game.load.audio("Ashot", ["assets/shot.mp3"]);
        game.load.audio("Adeath", ["assets/death.mp3"]);
        game.load.audio("Ahurt", ["assets/hurt.mp3"]);
        game.load.audio("Ajump", ["assets/jump.mp3"]);
        game.load.audio("Aland", ["assets/land.mp3"]);
        game.load.audio("Aover", ["assets/over.mp3"]);
        game.load.audio("Abreak", ["assets/break.mp3"]);

        game.load.spritesheet("player", "assets/cat.png", 31, 37);
        game.load.spritesheet("escalator", "assets/escalator.png", 150, 30);

        game.load.image("normal_block", "assets/normal_block.png", 30, 30);
        game.load.image("wall", "assets/wall.png", 30, 30);
        game.load.image("spike", "assets/spike.png", 30, 30);
        game.load.image("spring", "assets/spring.png", 30, 30);
        game.load.image("spring_temp", "assets/spring_temp.png", 30, 30);
        game.load.image("ceil", "assets/ceil.png", 30, 30);
        game.load.image("cloud", "assets/cloud.png", 70, 40);
        game.load.image("cloud_evil", "assets/cloud_evil.png", 70, 40);
        //game.load.image("mt", "assets/mt.png", 150, 74);
        //game.load.image("mt", "assets/mtc.png", 150, 74);
        //game.load.image("mtexp", "assets/mtexp.png", 600, 300);
        game.load.image("mtexp", "assets/mtd.png", 150 * 4, 74 * 4);
        game.load.image("grass", "assets/grass.png", 66, 27);
        game.load.image("title", "assets/title.png", 765, 180);
        game.load.image("layer", "assets/layer.png", 600, 600);
        game.load.image("ene1", "assets/ene1.png", 32, 44);
    },

    create: function () {
        gAOver = game.add.audio("Aover");
        game.state.start("title");
    }
};








var titleState = {
    preload: function () {

    },

    create: function () {
        game.renderer.renderSession.roundPixels = true;
        //game.stage.backgroundColor = "#91ff72";
        game.stage.backgroundColor = "#c3f5be";
        this.wallOffset = 0;
        this.selection = 0;


        game.input.keyboard.addKey(Phaser.Keyboard.UP).onDown.add(() => {
            this.selection--;
        }, this);

        game.input.keyboard.addKey(Phaser.Keyboard.DOWN).onDown.add(() => {
            this.selection++;
        }, this);




        this.wall = game.add.group();
        this.wall.add(game.add.tileSprite(0, 0, 30, kScreenSize[1] * 2, "wall"));
        this.wall.add(game.add.tileSprite(kScreenSize[0] - 30, 0, 30, kScreenSize[1] * 2, "wall"));


        this.block = game.add.group();

        var create_platform = (y, first) => {
            var x, y;
            if (first) {
                x = game.width / 2 - 75;
                y = game.height / 2 + 30;
            } else {
                x = Math.random() * (kScreenSize[0] - 30 * 2 - 35 * 2 - 150) + 65;
                y = (Math.random() * 2 - 1) * 10 + y;
            }

            var rnd = Math.random();
            if (rnd > 0.85) {
                var p = game.add.tileSprite(x, y, 150, 30, "normal_block");
                p.addChild(game.add.tileSprite(0, -15, 150, 15, "spike"));
                p.mType = "spike";
            } else if (rnd > 0.7) {
                var p = game.add.tileSprite(x, y, 150, 30, "spring");
                p.mType = "spring";
            } else if (rnd > 0.55) {
                if (rnd * 100 % 10 > 5) {
                    var p = game.add.sprite(x + 150, y, "escalator");
                    p.scale.x = -1;
                } else {
                    var p = game.add.sprite(x, y, "escalator");
                }
                p.mType = "escalator";
                p.animations.add("moving", [0, 1, 2, 3], 8, true);
                p.animations.play("moving");
            } else {
                var p = game.add.tileSprite(x, y, 150, 30, "normal_block");
                p.mType = "normal";
            }

            this.block.add(p);
        };




        create_platform(0, true);
        create_platform(400);
        create_platform(500);
        create_platform(600);

        game.time.events.loop(1000 / 60 / gStep * 100, () => {

            let arr = this.block.children;
            for (let i = arr.length - 1; i >= 0; i--) {
                let obj = arr[i];

                if (obj.y < -50) {
                    obj.destroy();
                    this.block.remove(obj);
                }
            }

            create_platform(600);

        }, this);

        this.ceil = game.add.tileSprite(30, 0, kScreenSize[0] - 60, 30, "ceil");




        this.title = game.add.sprite(300, 200, "title");
        this.title.anchor.setTo(0.5, 0.5);
        this.title.scale.setTo(0.7, 0.7);

        var start = game.add.text(300, 420, "Start", { fill: "white", fontSize: "40px" });
        start.anchor.setTo(0.5, 0.5);

        var hard = game.add.text(300, 470, "Hardcore Mode", { fill: "white", fontSize: "40px" });
        hard.anchor.setTo(0.5, 0.5);

        var rank = game.add.text(300, 520, "Rank", { fill: "#ffcf4f", fontSize: "40px" });
        rank.anchor.setTo(0.5, 0.5);

        this.options = [start, hard, rank];
    },

    update: function () {
        this.block.setAll("y", -gStep, false, false, 1);

        this.wallOffset++;
        if (this.wallOffset < 90) {
            this.wall.setAll("y", -1, false, false, 1);
        } else {
            this.wall.setAll("y", 0, false, false, 0);
            this.wallOffset = 0;
        }




        if (this.selection > 2) {
            this.selection = 2;
        } else if (this.selection < 0) {
            this.selection = 0;
        }

        this.options.forEach((v, i) => {
            v.fill = "#ffcf4f";

            if (i == this.selection) {
                v.fill = "white";
            }
        });

        if (game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            switch (this.selection) {
                case 0:
                    gHardMode = false;
                    game.state.start("main");
                    break;
                case 1:
                    gHardMode = true;
                    game.state.start("main");
                    break;
                case 2:
                    game.state.start("rank");
                    break;
            }
        }
    }
}








var rankState = {
    preload: function () {
        this.data = [];
        var _this = this;

        var title = game.add.text(300, 80, "Rank", { fill: "#ffcf4f", fontSize: "50px", align: "center" });
        title.anchor.setTo(0.5, 0.5);


        game.add.text(40, 100, "Normal", { fill: "yellow", fontSize: "40px" }).anchor.setTo(0, 0);
        var rank_n = game.add.text(40, 150, "", { fill: "white", fontSize: "30px", stroke: "black", strokeThickness: 5 });
        rank_n.anchor.setTo(0, 0);

        var rank_s = game.add.text(270, 150, "", { fill: "white", fontSize: "30px", stroke: "black", strokeThickness: 5, align: "right" });
        rank_s.anchor.setTo(1, 0);


        game.add.text(560, 100, "Hardcore", { fill: "yellow", fontSize: "40px", align: "right" }).anchor.setTo(1, 0);
        var rank_hn = game.add.text(330, 150, "", { fill: "white", fontSize: "30px", stroke: "black", strokeThickness: 5 });
        rank_n.anchor.setTo(0, 0);

        var rank_hs = game.add.text(560, 150, "", { fill: "white", fontSize: "30px", stroke: "black", strokeThickness: 5, align: "right" });
        rank_hs.anchor.setTo(1, 0);


        var desc = game.add.text(300, 530, "Press Enter to return to the title", { fill: "white", fontSize: "30px" });
        desc.anchor.setTo(0.5, 0.5);

        var loading = game.add.text(300, 300, "Loading", { fill: "white", fontSize: "30px", stroke: "black", strokeThickness: 5 });
        loading.anchor.setTo(0.5, 0.5);




        firebase.database().ref("normal_mode").orderByChild("score").limitToLast(8).once("value").then((snapshot) => {
            let arr = [];

            snapshot.forEach(o => {
                arr.push(o.val());
            });

            arr.reverse();

            let name = "";
            let score = "";

            for (let i = 0; i < arr.length; i++) {
                name += (arr[i].userName).slice(0, 8);
                name += "\n";

                score += arr[i].score.toString().slice(-5);
                score += "\n";
            }
            rank_n.text = name;
            rank_s.text = score;

            loading.destroy();
        });

        firebase.database().ref("hardcore_mode").orderByChild("score").limitToLast(8).once("value").then((snapshot) => {
            let arr = [];

            snapshot.forEach(o => {
                arr.push(o.val())
            });

            arr.reverse();

            let name = "";
            let score = "";

            for (let i = 0; i < arr.length; i++) {
                name += (arr[i].userName).slice(0, 8);
                name += "\n";

                score += arr[i].score.toString().slice(-5);
                score += "\n";
            }
            rank_hn.text = name;
            rank_hs.text = score;

            loading.destroy();
        });
    },

    create: function () {
    },

    update: function () {
        if (game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            game.state.start("title");
        }
    }
};








var mainState = {
    preload: function () {
    },

    create: function () {
        gScore = 0;

        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        //game.stage.backgroundColor = "#91ff72";
        game.stage.backgroundColor = "#c3f5be";
        this.cursor = game.input.keyboard.createCursorKeys();
        this.wallOffset = 0;
        this.endGame = false;

        game.input.keyboard.addKey(Phaser.Keyboard.ENTER).onDown.add(() => {
            if (!this.endGame && game.paused) {
                clearInterval(this.layerTimer);

                this.layerTimer = setInterval((() => {
                    this.layer.alpha = Math.max(0, this.layer.alpha - 0.02);
                    this.ptitle.alpha = Math.min(0, this.ptitle.alpha - 0.04);
                    this.pdesc.alpha = Math.min(0, this.pdesc.alpha - 0.04);

                    if (this.layer.alpha < 0.015) {
                        clearInterval(this.layerTimer);
                        this.layer.destroy();
                        this.ptitle.destroy();
                        this.pdesc.destroy();
                        game.paused = !game.paused;

                        game.state.start("title");
                    }
                }).bind(this), 1);
            }
        }, this);

        game.input.keyboard.addKey(Phaser.Keyboard.ESC).onDown.add(() => {
            if (!this.endGame) {

                if (!game.paused) {
                    game.paused = !game.paused;

                    this.layer = game.add.sprite(0, 0, "layer");
                    this.layer.alpha = 0;

                    this.ptitle = game.add.text(300, 150, "Pause", { fill: "#ffcf4f", fontSize: "70px" });
                    this.pdesc = game.add.text(300, 450, "Press ESC to continue the game\nPress Enter to return to the title", { fill: "white", fontSize: "30px" });
                    this.ptitle.anchor.setTo(0.5, 0.5);
                    this.pdesc.anchor.setTo(0.5, 0.5);
                    this.ptitle.alpha = 0;
                    this.pdesc.alpha = 0;

                    this.layerTimer = setInterval((() => {
                        this.layer.alpha = Math.min(0.5, this.layer.alpha + 0.02);
                        this.ptitle.alpha = Math.min(1.0, this.ptitle.alpha + 0.04);
                        this.pdesc.alpha = Math.min(1.0, this.pdesc.alpha + 0.04);

                        if (this.layer.alpha > 0.47) {
                            clearInterval(this.layerTimer);
                        }
                    }).bind(this), 1);

                } else {
                    clearInterval(this.layerTimer);

                    this.layerTimer = setInterval((() => {
                        this.layer.alpha = Math.max(0, this.layer.alpha - 0.02);
                        this.ptitle.alpha = Math.min(0, this.ptitle.alpha - 0.04);
                        this.pdesc.alpha = Math.min(0, this.pdesc.alpha - 0.04);

                        if (this.layer.alpha < 0.015) {
                            clearInterval(this.layerTimer);
                            this.layer.destroy();
                            this.ptitle.destroy();
                            this.pdesc.destroy();
                            game.paused = !game.paused;
                        }
                    }).bind(this), 1);

                }
            }
        }, this);




        this.bg = game.add.group();
        game.time.events.loop(2000, () => {

            let arr = this.bg.children;
            for (let i = arr.length - 1; i >= 0; i--) {
                let obj = arr[i];

                if (obj.y < 50) {

                    var f = game.add.tween(obj);
                    f.to({ alpha: 0 }, Math.random() * 1700 + 700, Phaser.Easing.Quadratic.In);
                    f.onComplete.add(() => {
                        obj.destroy();
                        this.bg.remove(obj);
                    }, this);

                    f.start();
                }
            }

            arr = this.bgf.children;
            for (let i = arr.length - 1; i >= 0; i--) {
                let obj = arr[i];

                if (obj.y < -50) {
                    obj.destroy();
                    this.bgf.remove(obj);
                }
            }

            let x1 = Math.random() * 200 - 300;
            let x2 = Math.random() * 200 + 100;

            let s1 = Math.random() * 2 + 5
            s1 = Math.random() * 0.5 + 1;
            let s2 = Math.random() * 2 + 5;
            s2 = Math.random() * 0.5 + 1;
            let s3 = Math.random() * 1 + 1;

            let m1 = game.add.sprite(x1, 600, "mtexp");
            let m2 = game.add.sprite(x2, 800 + Math.random() * 100, "mtexp");
            //let m1 = game.add.sprite(0, 600, "mtexp");
            //let m2 = game.add.sprite(0, 800 + Math.random() * 100, "mtexp");
            this.bg.add(m1);
            this.bg.add(m2);
            m1.scale.setTo(s1, s1);
            m2.scale.setTo(s2, s2);

            let c = game.add.sprite(Math.random() * (540 - 70 * s3) + 30, 700 + (Math.random() * 2 - 1) * 100, "cloud");
            if (gHardMode && Math.random() < 0.2) {
                game.physics.arcade.enable(c);
                c.body.immovable = true;
                c.mType = "cloud";
                c.mV = [0, 0];

                if (Math.random() < 0.3) {
                    c.alpha = 0;
                }
            }
            this.bgf.add(c);
            c.scale.setTo(s3, s3);

            if (gHardMode && Math.random() < 0.4) {
                playAudio(this, "Ashot");
                for (let i = 0; i < 5 + Math.random() * 4; i++) {
                    let o = game.add.sprite(Math.random() * 400 + 100, 700, "ene1");
                    o.scale.setTo(2, 2);
                    o.angle = Math.random() * 180 - 90;
                    game.physics.arcade.enable(o);

                    let angle = (o.angle - 90) * -1;
                    o.body.velocity.x = Math.cos(angle * 2 * Math.PI / 360) * (Math.random() * 100 + 300);
                    o.body.velocity.y = -Math.sin(angle * 2 * Math.PI / 360) * (Math.random() * 100 + 300);
                    o.mType = "ene1";
                    o.mV = [o.body.velocity.x, o.body.velocity.y];

                    this.bgf.add(o);
                }
            }

        }, this);



        this.wall = game.add.group();
        this.wall.enableBody = true;
        this.wall.add(game.add.tileSprite(0, 0, 30, kScreenSize[1] * 2, "wall"));
        this.wall.add(game.add.tileSprite(kScreenSize[0] - 30, 0, 30, kScreenSize[1] * 2, "wall"));
        this.wall.setAll("body.immovable", true);




        this.block = game.add.group();
        this.block.enableBody = true;

        var create_platform = (y, first) => {
            if (this.endGame) {
                return;
            }

            var x, y;
            var rnd = Math.random();
            if (first) {
                x = game.width / 2 - 75;
                y = game.height / 2 + 30;
                rnd = 0.21;
            } else {
                //x = Math.random() * (kScreenSize[0] - 30 * 2 - 35 * 2 - 150) + 65;
                x = Math.random() * (kScreenSize[0] - 30 * 2 - 150) + 30;
                y = (Math.random() * 2 - 1) * 10 + y;
            }
            
            var base = (Math.min(0.55, gScore / 300) + 0.45) * 1.0;
            var bound = [1 - base / 2, 1 - base / 4 * 3, 1 - base];

            if (rnd > bound[0]) {
                var p = game.add.tileSprite(x, y, 150, 30, "normal_block");
                p.addChild(game.add.tileSprite(0, -15, 150, 15, "spike"));
                p.mType = "spike";
            } else if (rnd > bound[1]) {
                var p = game.add.tileSprite(x, y, 150, 30, "spring");
                p.mType = "spring";
            } else if (rnd > bound[2]) {
                if (rnd * 100 % 10 > 5) {
                    var p = game.add.sprite(x + 150, y, "escalator");
                    p.scale.x = -1;
                } else {
                    var p = game.add.sprite(x, y, "escalator");
                }
                p.mType = "escalator";
                p.animations.add("moving", [0, 1, 2, 3], 8, true);
                p.animations.play("moving");
            } else {
                var p = game.add.tileSprite(x, y, 150, 30, "normal_block");
                p.mType = (rnd < 0.2 && gHardMode && !first) ? "normal_evil" : "normal";
            }

            this.block.add(p);
            p.body.immovable = true;
            p.body.checkCollision.down = false;
            p.body.checkCollision.right = false;
            p.body.checkCollision.left = false;
        };




        create_platform(0, true);
        create_platform(400);
        create_platform(500);
        create_platform(600);

        game.time.events.loop(1000 / 60 / gStep * 100, () => {

            let arr = this.block.children;
            for (let i = arr.length - 1; i >= 0; i--) {
                let obj = arr[i];

                if (obj.y < -50) {
                    obj.destroy();
                    this.block.remove(obj);
                }
            }

            create_platform(600);

        }, this);




        this.player = game.add.sprite(game.width / 2, game.height / 2, 'player');
        game.physics.arcade.enable(this.player);
        this.player.anchor.setTo(0.5, 0.5);
        this.player.scale.setTo(kPlayerScale[0], kPlayerScale[1]);

        this.player.animations.add('walk', [0, 1], 8, true);
        this.player.animations.add('falling', [3], 8, true);
        this.player.idleFrame = 0;

        this.player.body.setSize(16, 7, 9, 31);
        this.player.body.gravity.y = 600;
        this.player.mLife = 10;




        this.bgf = game.add.group();
        this.ceil = game.add.tileSprite(30, 0, kScreenSize[0] - 60, 30, "ceil");

        this.live = game.add.text(40, 30, "HP : 10", { fill: "white", stroke: "black", strokeThickness: 5 });//{ fill: "white", stroke: "#faa71a", strokeThickness: 5 });

        this.floor = game.add.text(560, 30, "B 0000F", { fill: "white", stroke: "black", strokeThickness: 5 });
        this.floor.anchor.setTo(1, 0);
    },

    update: function () {

        var heal = function (value, platform) {
            if (!platform.mLifeTag) {

                if (platform.mType == "spring_temp") {
                    playAudio(this, "Ajump");
                } else if (platform.mType == "escalator") {

                } else {
                    playAudio(this, "Aland");
                }

                this.player.mLife += value;
                platform.mLifeTag = true;
                if (this.player.mLife > 10) {
                    this.player.mLife = 10;
                }
                this.live.text = "HP : " + this.player.mLife;
            }
        }.bind(this);

        var damage = function (value, platform) {
            if (!platform.mLifeTag) {
                if (this.player.mLife <= value) {
                    this.live.text = "HP : 0";
                    this.gameOver();
                } else {
                    let fade = game.add.tween(this.player).to({ alpha: 0.1 }, 50, Phaser.Easing.Linear.None, true, 0, 12).yoyo(true);
                    fade.onComplete.add(() => {
                        this.player.alpha = 1;
                        if (platform == this.ceil) {
                            this.ceil.mLifeTag = 0;
                        }
                    }, this);

                    platform.mLifeTag = true;
                    this.player.mLife -= value;
                    this.live.text = "HP : " + this.player.mLife;

                    playAudio(this, "Ahurt");
                }
            }
        }.bind(this);




        if (this.endGame) {
            return;
        }

        if (this.player.y < 10) {
            this.player.y = 10;
        } else if (this.player.y < 45 && !this.player.mSuper) {
            damage(4, this.ceil);
        } else if (!this.player.inWorld) {
            this.gameOver();
        }




        game.physics.arcade.collide(this.player, this.wall);

        game.physics.arcade.collide(this.player, this.block, (player, block) => {
            if (player.y + 14 < block.y && player.body.velocity.y >= 0) {
                if (block.mType == "spring") {
                    block.mType = "spring_temp";
                    block.loadTexture("spring_temp");
                    player.body.velocity.y = -300;

                    var bounce = game.add.tween(block);
                    bounce.to({ y: block.y - 70 * 60 / 1000 * gStep + 15 }, 70, Phaser.Easing.Quadratic.Out).to({ y: block.y - 70 * 60 / 1000 * gStep - 15 }, 70, Phaser.Easing.Quadratic.In);
                    bounce.onComplete.add(() => {
                        block.mLifeTag = null;
                        block.mType = "spring";
                        block.loadTexture("spring");
                    }, this);

                    bounce.start();
                    heal(1, block);
                }
                else if (block.mType == "escalator") {
                    player.x += 2.5 * ((block.scale.x > 0) * 2 - 1);

                    if (player.x < 45) {
                        player.x = 45;
                    } else if (player.x > 555) {
                        player.x = 555
                    }

                    heal(1, block);
                } else if (block.mType == "spike") {
                    damage(4, block);
                } else if (block.mType == "normal_evil") {
                    let x = block.x;
                    let y = block.y;

                    let f = game.add.tween(block);
                    f.to({ alpha: 0 }, 1000, Phaser.Easing.Quadratic.Out);
                    f.onComplete.add(() => {
                        this.block.remove(block);
                        block.destroy();
                    }, this);
                    f.start();

                    block.mType = "normal_evil_temp";
                    this.block.add(game.add.text(x + Math.random() * 30 - 60, y + Math.random() * 30 - 60, gLaugh[Math.floor(Math.random() * gLaugh.length)], { fill: "white", fontSize: "20px", stroke: "black", strokeThickness: 5 }));
                    block.body.setSize(0, 0, 0, 0);
                    playAudio(this, "Abreak");
                } else {
                    heal(1, block);
                }
            }
        }, null, this);

        let temp = this.player.body.velocity.y;

        game.physics.arcade.collide(this.player, this.bgf, (player, ene) => {
            ene.addChild(game.add.text(Math.random() * 30 - 60, Math.random() * 30 - 60, gLaugh[Math.floor(Math.random() * gLaugh.length)], { fill: "white", fontSize: "20px", stroke: "black", strokeThickness: 5 }));
            damage(4, ene);
            ene.body.setSize(0, 0, 0, 0);
            ene.body.velocity.x = ene.mV[0];
            ene.body.velocity.y = ene.mV[1];

            if (ene.mType == "cloud") {
                ene.alpha = 1;
                ene.loadTexture("cloud_evil");
            }
        }, null, this);




        if (this.endGame) {
            return;
        }

        this.player.body.velocity.y = temp;

        this.block.setAll("y", -gStep, false, false, 1);
        this.bgf.setAll("y", -2, false, false, 1);
        this.bg.setAll("y", -2, false, false, 1);

        this.wallOffset++;
        if (this.wallOffset < 90) {
            this.wall.setAll("y", -1, false, false, 1);
        } else {
            this.wall.setAll("y", 0, false, false, 0);
            this.wallOffset = 0;

            gScore++;
            this.floor.text = "B " + "0".repeat(4 - gScore.toString().length) + gScore + "F";
        }

        this.movePlayer();
    },

    gameOver: function () {
        if (this.endGame) {
            return;
        }
        this.endGame = true;

        var sound = game.add.audio("Adeath");
        sound.play();
        sound.onStop.add(() => {
            if (this.endGame) {
                game.state.start("result");
                this.endGame = false;
                sound.destroy();
            }
        });

        this.player.body.velocity.x = 0;
        if (this.player.y < 200) {
            this.player.body.velocity.y = 0;
        } else {
            this.player.body.velocity.y = -550;
        }
    },

    movePlayer: function () {
        var moving = this.cursor.left.isDown || this.cursor.right.isDown;

        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -kPlayerSpeed;
            this.player.scale.setTo(-kPlayerScale[0], kPlayerScale[1]);
        }
        else if (this.cursor.right.isDown) {
            this.player.body.velocity.x = kPlayerSpeed;
            this.player.scale.setTo(kPlayerScale[0], kPlayerScale[1]);
        } else {
            this.player.body.velocity.x = 0;
        }

        if (!this.player.body.touching.down) {
            this.player.animations.play("falling");
        } else if (moving) {
            this.player.animations.play("walk");
        } else {
            this.player.animations.stop();
            this.player.frame = 0;
        }
    }
};








var resultState = {
    preload: function () {
    },

    create: function () {
        game.stage.backgroundColor = "black";
        this.selection = 0;
        gAOver.play();


        game.input.keyboard.addKey(Phaser.Keyboard.UP).onDown.add(() => {
            this.selection--;
        }, this);

        game.input.keyboard.addKey(Phaser.Keyboard.DOWN).onDown.add(() => {
            this.selection++;
        }, this);


        this.title = game.add.text(300, 150, "Game Over", { fill: "#ffcf4f", fontSize: "50px" });
        this.title.anchor.setTo(0.5, 0.5);

        this.floor = game.add.text(300, 250, "B " + "0".repeat(4 - gScore.toString().length) + gScore + "F", { fill: "white", fontSize: "30px" });
        this.floor.anchor.setTo(0.5, 0.5);

        var cont = game.add.text(300, 450, "Play again", { fill: "white", fontSize: "30px" });
        cont.anchor.setTo(0.5, 0.5);

        var back2title = game.add.text(300, 500, "Back to the title", { fill: "#ffcf4f", fontSize: "30px" });
        back2title.anchor.setTo(0.5, 0.5);

        this.options = [cont, back2title];




        // TODO: mode
        var ref = firebase.database().ref(gHardMode ? "hardcore_mode" : "normal_mode").push().set({
            userName: gName,
            score: gScore
        });
    },

    update: function () {
        if (this.selection > 1) {
            this.selection = 1;
        } else if (this.selection < 0) {
            this.selection = 0;
        }

        this.options.forEach((v, i) => {
            v.fill = "#ffcf4f";

            if (i == this.selection) {
                v.fill = "white";
            }
        });

        if (game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            game.sound.stopAll();
            switch (this.selection) {
                case 0:
                    game.state.start("main");
                    break;
                case 1:
                    game.state.start("title");
                    break;
            }
        }
    }
}







var game = new Phaser.Game(kScreenSize[0], kScreenSize[1], Phaser.AUTO, 'canvas');
game.state.add("boot", bootState);
game.state.add("load", loadState);
game.state.add("title", titleState);
game.state.add("rank", rankState);
game.state.add("main", mainState);
game.state.add("result", resultState);
game.state.start("boot");
//Phaser.Canvas.setSmoothingEnabled(game.context, false)